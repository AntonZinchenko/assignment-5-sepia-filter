#include "c_realization/lib/include/processinc_bmp.h"
#include "c_realization/sepia_filter.h"
#include <time.h>

//extern void sepia_filter_asm(uint64_t width, uint64_t height, struct pixel* data, struct pixel* new_data);
extern void sepia_filter_asm(struct image* img, struct image* new_img);

int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    struct image img;
    enum read_status read_pic_status = read_from_bmp(&img, "in.bmp");
    processing_read_status(read_pic_status);

    float c_count = 0;
    float asm_count = 0;
    for (size_t i = 0; i < 100; i++) {
        // Проверка времени для реализации на C
        struct pixel *new_data_c = malloc(sizeof(struct pixel) * img.width * img.height);
        struct image new_img_c = (struct image) {.width = img.width, .height = img.height, .data = new_data_c};

        clock_t start_c = clock();
        sepia_filter_c(&img, &new_img_c);
        clock_t end_c = clock();
        //printf("C: %" PRIu64 "\n", end_c - start_c);
        c_count += end_c - start_c;

        enum write_status write_pic_status_c = write_to_bmp(&new_img_c, "c_out.bmp");
        processing_write_status(write_pic_status_c);

        // Проверка времени для реализации на ассемблере
        struct pixel *new_data_asm = malloc(sizeof(struct pixel) * img.width * img.height);
        struct image new_img_asm = (struct image) {.width = img.width, .height = img.height, .data = new_data_asm};

        clock_t start_asm = clock();
        sepia_filter_asm(&img, &new_img_asm);
        clock_t end_asm = clock();
        //printf("Ассемблер: %" PRIu64 "\n", end_asm - start_asm);
        asm_count += end_asm - start_asm;

        enum write_status write_pic_status_asm = write_to_bmp(&new_img_asm, "asm_out.bmp");
        processing_write_status(write_pic_status_asm);
    }
    printf("Среднее время на 100 итерациях для C: %f \n", c_count/100);
    printf("Среднее время на 100 итерациях для ASM: %f \n", asm_count/100);
    printf("C/ASM: %f \n", c_count/asm_count);
}
#include "lib/include/image_inter.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef LAB5_SEPIA_FILTER_H
#define LAB5_SEPIA_FILTER_H

void sepia_filter_c(struct image* img, struct image* new_img);

#endif //LAB5_SEPIA_FILTER_H

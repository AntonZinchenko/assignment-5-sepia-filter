#include "sepia_filter.h"

uint8_t calc_color(struct pixel pixel, uint8_t type) {
    uint16_t color;
    switch (type) {
        case 0:
            color = 0.393*pixel.r + 0.769*pixel.g + 0.189*pixel.b;
            break;
        case 1:
            color = 0.349*pixel.r + 0.686*pixel.g + 0.168*pixel.b;;
            break;
        case 2:
            color = 0.272*pixel.r + 0.534*pixel.g + 0.131*pixel.b;
            break;
    }
    if (color > 255) {
        return 255;
    }
    return color;
}

void sepia_filter_c(struct image* img, struct image* new_img) {
    struct pixel pixel;
    struct pixel new_pixel;
    for (size_t i = 0; i < img -> height; i++) {
        for (size_t z = 0; z < img -> width; z++) {
            pixel = img -> data[img -> width * i + z];
            new_pixel.r = calc_color(pixel, 0);
            new_pixel.g = calc_color(pixel, 1);
            new_pixel.b = calc_color(pixel, 2);
            new_img -> data[img -> width * i + z] = new_pixel;
        }
    }
}